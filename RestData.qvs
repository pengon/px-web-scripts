/*
Hakee PX-Web rajapinnasta Qlik REST Connectorilla dataa ja laittaa sen suoraan tauluun
Connectorin nimen on löydyttävä RestConnection -muuttujasta

Lopullinen taulu sisältää eri muuttujat (Dims) ja arvot (Measure) omissa kentissään.
Dims -kenttä sisältää eri dimensiot eroteltuna tabulaattorilla.
__KEY_data on pelkkä avainkenttä Dimensioiden ja Measureiden joinaamiseen.

Esim. Yrityskanta (Dims: Alue, Toimiala, Kvartaali, Vuosi, Tiedot):
__KEY_data  |	Dims                         		| Measure
	1		|	SSS	01	Q1	2013	Aloittaneita	|    224
	2		|	SSS	01	Q1	2013	Lopettaneita	|     69
	3		|	SSS	01	Q1	2013	Yrityskanta		|  10148
*/

LIB CONNECT TO '$(RestConnection)';

RestConnectorMasterTable:
SQL SELECT 
	"__KEY_root",
	(SELECT 
		"__FK_data",
		"__KEY_data",
		(SELECT 
			"@Value",
			"__FK_key"
		FROM "key" FK "__FK_key" ArrayValueAlias "@Value"),
		(SELECT 
			"@Value" AS "@Value_u0",
			"__FK_values"
		FROM "values" FK "__FK_values" ArrayValueAlias "@Value_u0")
	FROM "data" PK "__KEY_data" FK "__FK_data")
FROM JSON (wrap on) "root" PK "__KEY_root";

/*
Ladataan avaimet ja yhdistetään samalle riville tabilla (CHR(9)) eroteltuna
*/
RestData:
Load
	[__KEY_data],
    Concat([@Value], CHR(9), Sort) as Dims
    GROUP BY [__KEY_data];
LOAD	[@Value] AS [@Value],
	[__FK_key] AS [__KEY_data],
    RecNo() AS Sort
RESIDENT RestConnectorMasterTable
WHERE NOT IsNull([__FK_key]);


/*
Joinataan itse arvot avaimiin
*/
JOIN(RestData)
LOAD	[@Value_u0] AS Measure,
	[__FK_values] AS [__KEY_data]
RESIDENT RestConnectorMasterTable
WHERE NOT IsNull([__FK_values]);


DROP TABLE RestConnectorMasterTable;

LET Dims = FieldValue('Dims', 1);
Trace First row of dims:;
Trace $(Dims);